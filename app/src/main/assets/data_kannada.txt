{
	"consent": {
		"title": "ಒಪ್ಪಿಗೆ",
		"points": [
			"ನಾನು ನಮೂದಿಸಿದ ಮಾಹಿತಿಯು  ಅಧಿಕ ರಕ್ತದೊತ್ತಡದ ಸಂಶೋಧನೆಯ ಅಧ್ಯಯನಕ್ಕಾಗಿ ಬಳಸಲ್ಪಡುತ್ತದೆ ಎಂದು ನಾನು ಅರ್ಥಮಾಡಿಕೊಂಡಿದ್ದೇನೆ.",
			"ನಾನು  ಯಾವ  ಸಮಯದಲ್ಲಾದರೂ ಈ ಚಟುವಟಿಕೆಯನ್ನು ನಿಲ್ಲಿಸಬಹುದು ಮತ್ತು ನನಗೆ ಯಾವುದೇ ಪ್ರಶ್ನೆಗಳನ್ನು ಹೊಂದಿದ್ದರೆ,  ನಾನು ಆರೋಗ್ಯ ರಕ್ಷಣೆ ನೀಡುಗರಿಗೆ ಹೇಳಬಲ್ಲೆ ಮತ್ತು ಅವರು ಅಥವಾ ಅವಳು ಈ ಸಂಶೋಧನೆಯನ್ನು ನಡೆಸುತ್ತಿರುವ ವಿಶ್ವವಿದ್ಯಾನಿಲಯ ಅಥವಾ ಆಸ್ಪತ್ರೆಯನ್ನು ಸಂಪರ್ಕಿಸಲು ನನಗೆ ಸಹಾಯ ಮಾಡುತ್ತಾರೆ."
		],
        "agree_icon": "icons_artboard_2.png",
        "cancel_icon": "icons_artboard_1.png",
		"audio_file": "kannada.wav"
	},

	"sociodemographic_data": {
		"title": "ಸಾಮಾಜಿಕ ಡೆಮೊಗ್ರಫಿಕ್ ಮಾಹಿತಿ",
		"questions": [{
				"question": "ನಿಮ್ಮ ವಯಸ್ಸು ಎಷ್ಟು?",
				"audio_file": "KQ_02.wav",
				"answers": [{
						"answer": "1",
						"icon": "icons_artboard_5.png"
					},
					{
						"answer": "2",
						"icon": "icons_artboard_6.png"
					},
					{
						"answer": "3",
						"icon": "icons_artboard_7.png"
					},
					{
						"answer": "4",
						"icon": "icons_artboard_8.png"
					}
				]
			},
			{
				"question": "ನೀವು  ಪುರುಷರೊ ಅಥವಾ ಮಹಿಳೆಯೊ?",
				"audio_file": "KQ_03.wav",
				"answers": [{
						"answer": "1",
						"icon": "icons_artboard_9.png"
					},
					{
						"answer": "2",
						"icon": "icons_artboard_10.png"
					}
				]
			},
			{
				"question": "ನೀವು ವಿವಾಹಿತರೊ ಅಥವಾ ಅವಿವಾಹಿತರೊ?",
				"audio_file": "KQ_04.wav",
				"answers": [{
						"answer": "1",
						"icon": "icons_artboard_11.png"
					},
					{
						"answer": "2",
						"icon": "icons_artboard_12.png"
					}
				]
			},
            {
				"question": "ನಿಮ್ಮ ಮನೆಯಲ್ಲಿ ಎಷ್ಟು ಜನ ವಾಸಿಸುತ್ತಾರೆ?",
				"audio_file": "KQ_05.wav",
				"answer_type": "number"
            },
			{
				"question": "ನೀವು ವಿದ್ಯಾಭ್ಯಾಸ ಮಾಡಿದ್ದೀರಾ ",
				"audio_file": "KQ_06.wav",
				"answers": [{
						"answer": "1",
						"icon": "icons_artboard_2.png",
						"questions": [{
							"question": "ನಿಮ್ಮ ಗರಿಷ್ಠ ಶಿಕ್ಷಣ ಏನು?",
							"audio_file": "KQ_07.wav",
							"answers": [{
									"answer": "1",
									"icon": "icons_artboard_13.png"
								},
								{
									"answer": "2",
									"icon": "icons_artboard_14.png"
								},
								{
									"answer": "3",
									"icon": "icons_artboard_15.png"
								},
								{
									"answer": "4",
									"icon": "icons_artboard_16.png"
								},
								{
									"answer": "5",
									"icon": "icons_artboard_17.png"
								}
							]
						}]
					},
					{
						"answer": "2",
						"icon": "icons_artboard_1.png"
					}
				]
			},
			{
				"question": "ನೀವು ಕೆಲಸಕ್ಕೆ ಹೋಗುತ್ತಿದ್ದೀರಾ?",
				"audio_file": "KQ_08.wav",
				"answers": [{
						"answer": "1",
						"icon": "icons_artboard_2.png"
					},
					{
						"answer": "2",
						"icon": "icons_artboard_1.png"
					}
				]
			},
			{
				"question": "ನೀವು ಧೂಮಪಾನ ಮಾಡುತ್ತೀರಾ?",
				"audio_file": "KQ_09.wav",
				"answers": [{
						"answer": "1",
						"icon": "icons_artboard_2.png"
					},
					{
						"answer": "2",
						"icon": "icons_artboard_1.png"
					}
				]
			},
			{
				"question": "ನೀವು ತಂಬಾಕು ಸೇವನೆ  ಮಾಡುತ್ತೀರಾ?",
				"audio_file": "KQ_10.wav",
				"answers": [{
						"answer": "1",
						"icon": "icons_artboard_2.png"
					},
					{
						"answer": "2",
						"icon": "icons_artboard_1.png"
					}
				]
			},
			{
				"question": "ನೀವು ಮಧ್ಯಪಾನ ಮಾಡುತ್ತೀರಾ?",
				"audio_file": "KQ_11.wav",
				"answers": [{
						"answer": "1",
						"icon": "icons_artboard_2.png"
					},
					{
						"answer": "2",
						"icon": "icons_artboard_1.png"
					}
				]
			},
			{
				"question": "ನಿಮಗೆ ಹೈ ಬಿಪಿ ಅಂದರೇ ಅಧಿಕ ರಕ್ತದೊತ್ತಡ ಇದೆಯಾ?",
				"audio_file": "KQ_12.wav",
				"answers": [{
						"answer": "1",
						"icon": "icons_artboard_2.png"
					},
					{
						"answer": "2",
						"icon": "icons_artboard_1.png"
					}
				]
			}
		]
	},

	"video_files": [{
			"title": "ಹೈ ಬಿಪಿ ಎಂದರೇನು?",
			"file_name": "htn_kpart1.mp4"
		},
		{
			"title": "ಅಧಿಕ ಬಿಪಿ ಏಕೆ ಉಂಟಾಗುತ್ತದೆ?",
			"file_name": "htn_kpart2.mp4"
		},
		{
			"title": "ನನಗೆ ಅಧಿಕ ರಕ್ತದೊತ್ತಡ ಇದ್ದಲ್ಲಿ ಏನಾಗುತ್ತದೆ?",
			"file_name": "htn_kpart3.mp4"
		},
		{
			"title": "ನಾನು ಈಗ ಏನು ಮಾಡಬೇಕು?",
			"file_name": "htn_kpart4.mp4"
		}
	],

	"questionnaire": [{
			"title": "ಹೈ ಬಿಪಿ ಎಂದರೇನು?",
			"question": "ರಕ್ತ ನಾಳಗಳು ಕಿರಿದಾದಾಗ , ಹೃದಯಕ್ಕೆ ಹೆಚ್ಚು ಶ್ರಮ ಪಡಬೇಕಾಗುತ್ತದೆ.",
			"audio_file": "KQ_14.wav,KQ_15.wav",
			"answers": [{
					"answer": "1",
					"icon": "icons_artboard_2.png"
				},
				{
					"answer": "2",
					"icon": "icons_artboard_1.png"
				}
			],
			"correct_answer": 0
		},
		{
			"title": "ಅಧಿಕ ಬಿಪಿ ಏಕೆ ಉಂಟಾಗುತ್ತದೆ?",
			"question": "ಯಾವ ತಿನಿಸುಗಳಿಂದ  ಅಧಿಕ ಬಿಪಿ ಉಂಟಾಗುತ್ತದೆ ಅಥವಾ  ಬಿಪಿ ಇನ್ನೂ ತೀವ್ರಗೊಳ್ಳುತ್ತದೆ?",
			"audio_file": "KQ_16.wav,KQ_17.wav",
			"answers": [{
					"answer": "1",
					"icon": "icons_artboard_18.png"
				},
				{
					"answer": "2",
					"icon": "icons_artboard_19.png"
				},
				{
					"answer": "3",
					"icon": "icons_artboard_20.png"
				}
			],
			"answers_in_popup" : "yes",
			"correct_answer": 0
		},
		{
			"title": "ಅಧಿಕ ಬಿಪಿ ಏಕೆ ಉಂಟಾಗುತ್ತದೆ?",
			"question": "ಪ್ರತಿದಿನವೂ ವೇಗವಾಗಿ ನಡೆಯುವುದು ಅಧಿಕ ಬಿಪಿಯನ್ನು ತಡೆಗಟ್ಟುವ  ಉತ್ತಮ ವಿಧಾನಗಳಲ್ಲೊಂದು?",
			"audio_file": "KQ_18.wav",
			"answers": [{
					"answer": "1",
					"icon": "icons_artboard_2.png"
				},
				{
					"answer": "2",
					"icon": "icons_artboard_1.png"
				}
			],
			"correct_answer": 0
		},
		{
			"title": "ಅಧಿಕ ಬಿಪಿ ಏಕೆ ಉಂಟಾಗುತ್ತದೆ?",
			"question": "ಅಡಿಗೆ ತಯಾರಿಕೆಯ ಯಾವ ವಿಧಾನದಿಂದಾಗಿ ಅಧಿಕ ಬಿಪಿ ಉಂಟಾಗುತ್ತದೆ ಅಥವಾ ಬಿಪಿ ಇನ್ನೂ ತೀವ್ರಗೊಳ್ಳುತ್ತದೆ?",
			"audio_file": "KQ_19.wav",
			"answers": [{
					"answer": "1",
					"icon": "icons_artboard_21.png"
				},
				{
					"answer": "2",
					"icon": "icons_artboard_22.png"
				},
				{
					"answer": "3",
					"icon": "icons_artboard_23.png"
				}
			],
			"answers_in_popup" : "yes",
			"correct_answer": 0
		},
		{
			"title": "ಅಧಿಕ ಬಿಪಿ ಏಕೆ ಉಂಟಾಗುತ್ತದೆ?",
			"question": "ತಂಬಾಕು ವಿನಿಂದ ಅಧಿಕ ರಕ್ತದೊತ್ತಡ ಉಂಟಾಗಬಹುದೆ?",
			"audio_file": "KQ_20.wav",
			"answers": [{
					"answer": "1",
					"icon": "icons_artboard_2.png"
				},
				{
					"answer": "2",
					"icon": "icons_artboard_1.png"
				}
			],
			"correct_answer": 0
		},
		{
			"title": "ನನಗೆ ಅಧಿಕ ರಕ್ತದೊತ್ತಡ ಇದ್ದಲ್ಲಿ ಏನಾಗುತ್ತದೆ?",
			"question": "ಅಧಿಕ ರಕ್ತದೊತ್ತಡ ಇದ್ದಲ್ಲಿ ಲಕ್ಷಣಗಳು ಕಾಣಿಸುತ್ತವೆಯೇ ?",
			"audio_file": "KQ_21.wav,KQ_22.wav",
			"answers": [{
					"answer": "1",
					"icon": "icons_artboard_2.png"
				},
				{
					"answer": "2",
					"icon": "icons_artboard_1.png"
				}
			],
			"correct_answer": 1
		},
		{
			"title": "ನನಗೆ ಅಧಿಕ ರಕ್ತದೊತ್ತಡ ಇದ್ದಲ್ಲಿ ಏನಾಗುತ್ತದೆ?",
			"question": "ಹೃದಯಘಾತದ ಒಂದು ಸಾಮಾನ್ಯ ಲಕ್ಷಣ ಯಾವುದು?",
			"audio_file": "KQ_23.wav",
			"answers": [{
					"answer": "1",
					"icon": "icons_artboard_26.png"
				},
				{
					"answer": "2",
					"icon": "icons_artboard_25.png"
				},
				{
					"answer": "3",
					"icon": "icons_artboard_27.png"
				}
			],
			"answers_in_popup" : "yes",
			"correct_answer": 2
		},
		{
			"title": "ನನಗೆ ಅಧಿಕ ರಕ್ತದೊತ್ತಡ ಇದ್ದಲ್ಲಿ ಏನಾಗುತ್ತದೆ?",
			"question": "ಯಾರಿಗಾದರೂ ಸ್ಟ್ರೋಕ್  ಉಂಟಾದಲ್ಲಿ ಇವುಗಳಲ್ಲಿ ಏನು ಕಂಡು ಬರಬಹುದು?",
			"audio_file": "KQ_24.wav",
			"answers": [{
					"answer": "1",
					"icon": "icons_artboard_29.png"
				},
				{
					"answer": "2",
					"icon": "icons_artboard_27.png"
				},
				{
					"answer": "3",
					"icon": "icons_artboard_28.png"
				}
			],
			"answers_in_popup" : "yes",
			"correct_answer": 0
		},
		{
			"title": "ನಾನು ಈಗ ಏನು ಮಾಡಬೇಕು?",
			"question": "ನನಗೆ ಅಧಿಕ ರಕ್ತದೊತ್ತಡ ಇದೆಯೆ ಎಂಬುದನ್ನು ತಿಳಿಯುವ ಉತ್ತಮ ವಿಧಾನವೆಂದರೆ ದೇಹದಲ್ಲಿ ಅಸಾಮಾನ್ಯ ಅನುಭವಗಳ ಕುರಿತು ಗಮನವಿರಿಸುವುದು",
			"audio_file": "KQ_25.wav,KQ_26.wav",
			"answers": [{
					"answer": "1",
					"icon": "icons_artboard_2.png"
				},
				{
					"answer": "2",
					"icon": "icons_artboard_1.png"
				}
			],
			"correct_answer": 1
		},
		{
			"title": "ನಾನು ಈಗ ಏನು ಮಾಡಬೇಕು?",
			"question": "ಅಧಿಕ ರಕ್ತದೊತ್ತಡ ಕ್ಕಾಗಿ ನೀಡಲಾದ ಔಷಧಿಗಳನ್ನು ಪ್ರತಿದಿನವೂ ಸೇವಿಸಬೇಕಾಗುತ್ತದೆ",
			"audio_file": "KQ_27.wav",
			"answers": [{
					"answer": "1",
					"icon": "icons_artboard_2.png"
				},
				{
					"answer": "2",
					"icon": "icons_artboard_1.png"
				}
			],
			"correct_answer": 0
		}
	],

    "location_types": [
        "<insert>",
        "<insert>"
    ],

    "result_message": [
        {
            "range_start": 0,
            "range_end": 4,
            "message": "<insert>",
            "image": "better_luck.png"
        },
        {
            "range_start": 5,
            "range_end": 8,
            "message": "<insert>",
            "image": "good.png"
        },
        {
            "range_start": 9,
            "range_end": 10,
            "message": "<insert>",
            "image": "congrats.png"
        }
    ],

	"app_texts": {
		"yes": "ಹೌದು",
		"no": "ಇಲ್ಲ",
		"cancel": "ರದ್ದುಮಾಡಿ",
		"watch": "ವೀಕ್ಷಿಸಿ",
		"submit": "ಸಲ್ಲಿಸಿ",
		"export_data": "<insert>",

		"confirm_proceed": "ನೀವು  ಮುಂದುವರೆಯಲು ಖಚಿತವಾಗಿ ಬಯಸುವಿರಾ?",
		"confirm_start_new": "ಹೊಸ ಸಮೀಕ್ಷೆಯನ್ನು ಪ್ರಾರಂಭಿಸಿ?",
		"confirm_on_back": "ನಿಮ್ಮ ಪ್ರಗತಿ ಕಳೆದು ಹೋಗುತ್ತದೆ. ನೀವು ಹಿಂದಿರುಗಲು ಖಚಿತವಾಗಿ ಬಯಸುವಿರಾ?",

		"title_consent": "ಒಪ್ಪಿಗೆ",
		"title_location": "<insert>",
		"title_sociodemographic_data": "ಸಾಮಾಜಿಕ ಡೆಮೊಗ್ರಫಿಕ್ ಮಾಹಿತಿ",
		"title_pre_questions": "ಪೂರ್ವ ಜಾಗೃತಿ ಪ್ರಶ್ನೆಗಳು",
		"title_watch_video": "ವೀಕ್ಷಿಸು ದೃಶ್ಯ",
		"title_post_questions": "ನಂತರದ ಜಾಗೃತಿ ಪ್ರಶ್ನೆಗಳು",
		"title_results": "ಫಲಿತಾಂಶಗಳು",
		"title_thank_you": "ಸಮೀಕ್ಷೆ ಪೂರ್ಣಗೊಂಡಿದೆ",

		"location_type": "<insert>",
		"location_name": "<insert>",
		"continue": "<insert>",

		"answer": "<insert>",
		"update_answer": "<insert>",
		"select_answer": "<insert>",

        "start_pre_questions" : "ಮೊದಲು ಹೈ ಬ್ಲಡ್ ಪ್ರೆಷರ್  ಅಂದರೇ  ಬಿಪಿ ಬಗ್ಗೆ ನೀವು ಏನೆಲ್ಲ ತಿಳಿದಿದ್ದೀರಿ ಎಂಬುದನ್ನು ನೋಡೋಣ.",
        "start_pre_questions_audio_file" : "KQ_13.wav",
        "start_post_questions" : "ಈಗ, ಹೈ ಬ್ಲಡ್ ಪ್ರೆಷರ್  ಅಂದರೇ  ಬಿಪಿ ಬಗ್ಗೆ ನೀವು ಏನೆಲ್ಲ ತಿಳಿದಿದ್ದೀರಿ ಎಂಬುದನ್ನು ನೋಡೋಣ.",
        "start_post_questions_audio_file" : "KQ_13.wav",

		"view_full_video": "ಪೂರ್ಣ ದೃಶ್ಯ ವೀಕ್ಷಿಸಿ",
		"view_module_video": "ಮಾಡ್ಯೂಲ್ ದೃಶ್ಯ ವೀಕ್ಷಿಸಿ",
        
		"wrong_answer": "ನಿಮ್ಮ ಉತ್ತರವು ತಪ್ಪಾಗಿದೆ",
		"correct_answer": "ಸರಿಯಾದ ಉತ್ತರ",

		"your_score": "<insert>",

		"select": "ಆಯ್ಕೆಮಾಡಿ",
        
        "start_survey": "ಪ್ರಾರಂಭ",

         "start_new": "<insert>",

         "done": "<insert>"
	}
}