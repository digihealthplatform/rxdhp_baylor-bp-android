package in.lucidify.baylor.util;

import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.lucidify.baylor.BaylorApplication;

public class LToast {

    public static void success(String message) {
        Toasty.success(BaylorApplication.getContext(), message, Toast.LENGTH_LONG).show();
    }

    public static void warning(String message) {
        Toasty.warning(BaylorApplication.getContext(), message, Toast.LENGTH_LONG).show();
    }

    public static void error(String message) {
        Toasty.error(BaylorApplication.getContext(), message, Toast.LENGTH_LONG).show();
    }
}