package in.lucidify.baylor.util;

public class LConstants {
    public static final int CODE_WRITE_PERMISSION = 101;

    public static final boolean AUTO_SYNC_ENABLED = true;

    public static final String ADMIN_ID  = "admin";
    public static final String ADMIN_PWD = "baylor123";

    public static final String LANGUAGE_FILE = "languages.txt";

    public static final String FILES_PATH       = "/Baylor/";
    public static final String VIDEO_PATH       = FILES_PATH + "Videos/";
    public static final String OUTPUT_JSON_PATH = FILES_PATH + "Output_Data/";

    public static final String FILES_PATH_HIDDEN = "/.BaylorFiles/";
    public static final String INPUT_JSON_PATH   = FILES_PATH_HIDDEN + "Input_Json/";
    public static final String ICONS_PATH        = FILES_PATH_HIDDEN + "Icons/";
    public static final String AUDIO_PATH        = FILES_PATH_HIDDEN + "Audio/";
}