package in.lucidify.baylor.data.di.component;

import dagger.Component;
import in.lucidify.baylor.data.di.PerActivity;
import in.lucidify.baylor.data.di.module.ActivityModule;
import in.lucidify.baylor.ui.admin.ActivityAdminHome;
import in.lucidify.baylor.ui.login.ActivityCopyAssets;
import in.lucidify.baylor.ui.login.ActivityLogin;
import in.lucidify.baylor.ui.login.ActivityLoginCheck;
import in.lucidify.baylor.ui.user.ActivityLanguageSelection;
import in.lucidify.baylor.ui.user.ActivityPostQuestions;
import in.lucidify.baylor.ui.user.ActivityResult;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(ActivityLoginCheck activityLoginCheck);

    void inject(ActivityLogin activityLogin);

    void inject(ActivityAdminHome activityAdminHome);

    void inject(ActivityLanguageSelection activityLanguageSelection);

    void inject(ActivityPostQuestions activityPostQuestions);

    void inject(ActivityResult activityResult);

    void inject(ActivityCopyAssets activityCopyAssets);
}