package in.lucidify.baylor.data.table;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

import in.lucidify.baylor.data.database.BaylorDatabase;

@Table(database = BaylorDatabase.class)
public class TableSurveyData extends BaseModel implements Serializable {

    @Column(name = "survey_number")
    @PrimaryKey(autoincrement = true)
    private int surveyNo;

    @Column(name = "selected_language")
    private String selectedLanguage;

    @Column(name = "sociodemographic_data")
    private String sociodemographicData;

    @Column(name = "pre_questions")
    private String preQuestions;

    @Column(name = "post_questions")
    private String postQuestions;

    @Column(name = "video_watched")
    private String videoWatched;

    @Column(name = "location_type")
    private String locationType;

    @Column(name = "location_name")
    private String locationName;

    @Column(name = "surveyor_name")
    private String surveyorName;

    @Column(name = "survey_date_time")
    private String surveyDateTime;

    @Column(name = "trainer")
    private boolean trainer;

    @Column(name = "synced")
    private boolean synced;

    public int getSurveyNo() {
        return surveyNo;
    }

    public void setSurveyNo(int surveyNo) {
        this.surveyNo = surveyNo;
    }

    public String getSelectedLanguage() {
        return selectedLanguage;
    }

    public void setSelectedLanguage(String selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }

    public String getSociodemographicData() {
        return sociodemographicData;
    }

    public void setSociodemographicData(String sociodemographicData) {
        this.sociodemographicData = sociodemographicData;
    }

    public String getPreQuestions() {
        return preQuestions;
    }

    public void setPreQuestions(String preQuestions) {
        this.preQuestions = preQuestions;
    }

    public String getPostQuestions() {
        return postQuestions;
    }

    public void setPostQuestions(String postQuestions) {
        this.postQuestions = postQuestions;
    }

    public String getVideoWatched() {
        return videoWatched;
    }

    public void setVideoWatched(String videoWatched) {
        this.videoWatched = videoWatched;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getSurveyorName() {
        return surveyorName;
    }

    public void setSurveyorName(String surveyorName) {
        this.surveyorName = surveyorName;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public boolean isTrainer() {
        return trainer;
    }

    public void setTrainer(boolean trainer) {
        this.trainer = trainer;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }
}