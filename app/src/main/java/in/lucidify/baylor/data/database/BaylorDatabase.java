package in.lucidify.baylor.data.database;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = BaylorDatabase.NAME, version = BaylorDatabase.VERSION)
public class BaylorDatabase {
    public static final String DATABASE_NAME = "baylor.db";

    public static final String NAME    = "BaylorDatabase";
    public static final int    VERSION = 1;
}