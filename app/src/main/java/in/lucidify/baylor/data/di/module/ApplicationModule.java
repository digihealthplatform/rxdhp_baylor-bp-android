package in.lucidify.baylor.data.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;
import in.lucidify.baylor.data.database.BaylorDatabase;
import in.lucidify.baylor.data.di.ApplicationContext;
import in.lucidify.baylor.data.di.DatabaseInfo;
import in.lucidify.baylor.data.database.SharedPrefsHelper;

@Module
public class ApplicationModule {
    private final Application application;

    public ApplicationModule(Application app) {
        application = app;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return BaylorDatabase.DATABASE_NAME;
    }

    @Provides
    @DatabaseInfo
    Integer provideDatabaseVersion() {
        return 1;
    }

    @Provides
    SharedPreferences provideSharedPrefs() {
        return application.getSharedPreferences(SharedPrefsHelper.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }
}