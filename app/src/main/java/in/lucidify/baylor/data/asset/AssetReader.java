package in.lucidify.baylor.data.asset;

import android.os.Environment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import in.lucidify.baylor.BaylorApplication;
import in.lucidify.baylor.R;
import in.lucidify.baylor.data.DataManager;
import in.lucidify.baylor.data.item.ColumnQuestionAnswer;
import in.lucidify.baylor.data.item.ConsentData;
import in.lucidify.baylor.data.item.ItemQuestion;
import in.lucidify.baylor.data.item.ItemResultDialog;
import in.lucidify.baylor.data.item.SociodemographicData;
import in.lucidify.baylor.data.item.VideoFile;
import in.lucidify.baylor.data.table.TableSurveyData;
import in.lucidify.baylor.util.LLog;
import in.lucidify.baylor.util.LToast;
import in.lucidify.baylor.util.LUtils;

import static in.lucidify.baylor.util.LConstants.INPUT_JSON_PATH;
import static in.lucidify.baylor.util.LConstants.LANGUAGE_FILE;

public class AssetReader {

    private static int[] postQuestionQuestionAnswersIndex;

    private static long surveyStartTime;
    private static long surveyEndTime;

    private static String selectedLanguageString;

    private static String locationType;
    private static String locationName;

    public static final String KEY_YES    = "yes";
    public static final String KEY_NO     = "no";
    public static final String KEY_CANCEL = "cancel";
    public static final String KEY_WATCH  = "watch";
    public static final String KEY_SUBMIT = "submit";
    public static final String KEY_EXPORT = "export_data";

    public static final String KEY_CONFIRM_PROCEED   = "confirm_proceed";
    public static final String KEY_CONFIRM_START_NEW = "confirm_start_new";
    public static final String KEY_CONFIRM_ON_BACK   = "confirm_on_back";

    public static final String KEY_TITLE_CONSENT                   = "title_consent";
    public static final String KEY_TITLE_LOCATION                  = "title_location";
    public static final String KEY_TITLE_SOCIODEMOGRAPHIC_DATA     = "title_sociodemographic_data";
    public static final String KEY_START_PRE_QUESTIONS             = "start_pre_questions";
    public static final String KEY_START_PRE_QUESTIONS_AUDIO_FILE  = "start_pre_questions_audio_file";
    public static final String KEY_TITLE_PRE_QUESTIONS             = "title_pre_questions";
    public static final String KEY_TITLE_WATCH_VIDEO               = "title_watch_video";
    public static final String KEY_START_POST_QUESTIONS            = "start_post_questions";
    public static final String KEY_START_POST_QUESTIONS_AUDIO_FILE = "start_post_questions_audio_file";
    public static final String KEY_TITLE_POST_QUESTIONS            = "title_post_questions";
    public static final String KEY_TITLE_RESULTS                   = "title_results";
    public static final String KEY_TITLE_THANK_YOU                 = "title_thank_you";

    public static final String KEY_LOCATION_NAME        = "location_name";
    public static final String KEY_LOCATION_TYPE        = "location_type";
    public static final String KEY_SELECT_LOCATION_TYPE = "message_select_location_type";
    public static final String KEY_ENTER_LOCATION_NAME  = "message_enter_location_name";
    public static final String KEY_CONTINUE             = "continue";

    public static final String KEY_WRONG_ANSWER   = "wrong_answer";
    public static final String KEY_CORRECT_ANSWER = "correct_answer";

    public static final String KEY_VIEW_FULL_VIDEO   = "view_full_video";
    public static final String KEY_VIEW_MODULE_VIDEO = "view_module_video";
    public static final String KEY_START_SURVEY      = "start_survey";

    public static final String KEY_SELECT        = "select";
    public static final String KEY_ANSWER        = "answer";
    public static final String KEY_UPDATE_ANSWER = "update_answer";
    public static final String KEY_SELECT_ANSWER = "select_answer";

    public static final String KEY_DONE      = "done";
    public static final String KEY_START_NEW = "start_new";

    public static final String KEY_YOUR_SCORE = "your_score";

    private static JSONObject jsonLanguages;
    private static JSONObject jsonData;
    private static JSONObject jsonDataEnglish;
    private static JSONObject jsonAppTexts;
    private static JSONArray  videoFiles;

    private static ArrayList<ColumnQuestionAnswer> socioDemographicQuestionAnswers;
    private static ArrayList<ColumnQuestionAnswer> preQuestionUserAnswers;
    private static ArrayList<ColumnQuestionAnswer> postQuestionUserAnswers;

    private static String videoWatched;

    public static String getAppText(String key) {
        try {
            return jsonAppTexts.getString(key);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public static boolean init() {
        try {
            FileInputStream stream = new FileInputStream(new File(Environment.getExternalStorageDirectory(), INPUT_JSON_PATH + LANGUAGE_FILE));
            byte[]          buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            jsonLanguages = new JSONObject(new String(buffer));

            if (isJsonFilesValid()) {
                return true;
            }

        } catch (IOException | JSONException e) {
            LLog.printStackTrace(e);
            LToast.error(BaylorApplication.getContext().getString(R.string.invalid_apk));
        }

        return false;
    }

    public static String[] getLanguages() {
        try {
            JSONArray jsonArray = jsonLanguages.getJSONArray("languages");

            String[] languages = new String[jsonArray.length()];

            for (int i = 0; i < jsonArray.length(); i++) {
                languages[i] = jsonArray.getJSONObject(i).getString("language");
            }

            return languages;

        } catch (JSONException e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public static void setSelectedLanguage(int selectedLanguage) {

        String dataFileName = "";

        try {
            JSONArray jsonArray = jsonLanguages.getJSONArray("languages");

            selectedLanguageString = jsonArray.getJSONObject(selectedLanguage).getString("language_text");
            dataFileName = jsonArray.getJSONObject(selectedLanguage).getString("data_file");

            FileInputStream stream = new FileInputStream(new File(Environment.getExternalStorageDirectory(), INPUT_JSON_PATH + dataFileName));
            byte[]          buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            jsonData = new JSONObject(new String(buffer));

            stream = new FileInputStream(new File(Environment.getExternalStorageDirectory(), INPUT_JSON_PATH + "data_english.txt"));
            buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            jsonDataEnglish = new JSONObject(new String(buffer));
            jsonAppTexts = jsonData.getJSONObject("app_texts");

        } catch (IOException | JSONException e) {
            LLog.printStackTrace(e);
        }
    }

    private static boolean isJsonFilesValid() {

        String dataFileName = "";

        try {
            JSONArray jsonArray = jsonLanguages.getJSONArray("languages");

            for (int i = 0; i < jsonArray.length(); i++) {
                dataFileName = jsonArray.getJSONObject(i).getString("data_file");

                FileInputStream stream = new FileInputStream(new File(Environment.getExternalStorageDirectory(), INPUT_JSON_PATH + dataFileName));
                byte[]          buffer = new byte[stream.available()];
                stream.read(buffer);
                stream.close();

                JSONObject jsonObject = new JSONObject(new String(buffer));

                return true;
            }

        } catch (IOException | JSONException e) {
            LToast.error(BaylorApplication.getContext().getString(R.string.invalid_apk));
            LLog.printStackTrace(e);
        }

        return false;
    }

    public static JSONObject getJsonData() {
        return jsonData;
    }

    public static JSONObject getJsonDataEnglish() {
        return jsonDataEnglish;
    }

    public static ConsentData getConsent() {
        ConsentData consentData = new ConsentData();

        try {
            JSONObject jsonObject = jsonData.getJSONObject("consent");

            consentData.setTitle(jsonObject.getString("title"));
            JSONArray jsonArray = jsonObject.getJSONArray("points");

            String[] points = new String[jsonArray.length()];

            for (int i = 0; i < jsonArray.length(); i++) {
                points[i] = jsonArray.getString(i);
            }

            consentData.setPoints(points);
            consentData.setAudioFile(jsonObject.getString("audio_file"));
            consentData.setAgreeIcon(jsonObject.getString("agree_icon"));
            consentData.setCancelIcon(jsonObject.getString("cancel_icon"));

        } catch (JSONException e) {
            LLog.printStackTrace(e);
        }

        return consentData;
    }

    public static SociodemographicData getSocioDemographicQuestions(JSONObject jsonData) {
        SociodemographicData sociodemographicData = new SociodemographicData();

        try {
            JSONObject jsonObject = jsonData.getJSONObject("sociodemographic_data");

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson        gson        = gsonBuilder.create();
            sociodemographicData = gson.fromJson(jsonObject.toString(), SociodemographicData.class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return sociodemographicData;
    }

    public static ItemQuestion[] getPreQuestions(JSONObject jsonData) {
        ItemQuestion questions[] = null;

        try {
            JSONArray jsonArray = jsonData.getJSONArray("questionnaire");

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson        gson        = gsonBuilder.create();
            questions = gson.fromJson(jsonArray.toString(), ItemQuestion[].class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return questions;
    }

    public static ItemQuestion[] getPostQuestions(JSONObject jsonData) {
        return getPreQuestions(jsonData);
    }

    public static void setSocioDemographicAnswers(ArrayList<ColumnQuestionAnswer> questionAnswers) {
        socioDemographicQuestionAnswers = questionAnswers;
    }

    public static void setPreQuestionUserAnswers(ArrayList<ColumnQuestionAnswer> questionAnswers) {
        preQuestionUserAnswers = questionAnswers;
    }

    public static void setPostQuestionQuestionAnswers(ArrayList<ColumnQuestionAnswer> questionAnswers, int[] questionAnswersIndex) {
        postQuestionUserAnswers = questionAnswers;
        postQuestionQuestionAnswersIndex = questionAnswersIndex;
    }

    public static int[] getPostQuestionUserAnswers() {
        return postQuestionQuestionAnswersIndex;
    }

    public static void surveyStarted() {
        surveyStartTime = Calendar.getInstance().getTimeInMillis();
    }

    public static void surveyEnded() {
        surveyEndTime = Calendar.getInstance().getTimeInMillis();
    }

    public static void saveLocationDetails(String locationType, String locationName) {
        AssetReader.locationType = locationType;
        AssetReader.locationName = locationName;
    }

    public static void saveSurveyData(DataManager dataManager) {
        TableSurveyData tableSurveyData = new TableSurveyData();

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson        gson        = gsonBuilder.create();

        tableSurveyData.setSurveyDateTime(LUtils.getDateTime(surveyStartTime));

        tableSurveyData.setLocationType(locationType);
        tableSurveyData.setLocationName(locationName);
        tableSurveyData.setSurveyorName(dataManager.getLoggedInUsername());
        tableSurveyData.setTrainer(dataManager.isTrainer());
        tableSurveyData.setSelectedLanguage(selectedLanguageString);
        tableSurveyData.setSociodemographicData(gson.toJson(socioDemographicQuestionAnswers));
        tableSurveyData.setPreQuestions(gson.toJson(preQuestionUserAnswers));
        tableSurveyData.setPostQuestions(gson.toJson(postQuestionUserAnswers));
        tableSurveyData.setVideoWatched(videoWatched);

        dataManager.saveSurveyData(tableSurveyData);
    }

    public static String getVideoFileName(JSONObject jsonData) {
        String videoFileName = "";

        try {
            videoFileName = jsonData.getString("video_file");
        } catch (JSONException e) {
            LLog.printStackTrace(e);
        }

        return videoFileName;
    }

    public static VideoFile[] getVideoFiles(JSONObject jsonData) {
        VideoFile videoFiles[] = null;

        try {
            JSONArray jsonArray = jsonData.getJSONArray("video_files");

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson        gson        = gsonBuilder.create();
            videoFiles = gson.fromJson(jsonArray.toString(), VideoFile[].class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return videoFiles;
    }

    public static void setVideoWatched(String title) {
        videoWatched = title;
    }

    public static String[] getLocationTypes(JSONObject jsonData) {
        String locationTypes[] = null;

        try {
            JSONArray jsonArray = jsonData.getJSONArray("location_types");

            locationTypes = new String[jsonArray.length() + 1];
            locationTypes[0] = AssetReader.getAppText(AssetReader.KEY_SELECT);

            for (int i = 0; i < jsonArray.length(); i++) {
                locationTypes[i + 1] = jsonArray.getString(i);
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return locationTypes;
    }

    public static ItemResultDialog getResultDialog(JSONObject jsonData, int correctAnswersCount, int totalQuestions) {
        ItemResultDialog itemResultDialog = new ItemResultDialog();

        try {
            JSONArray jsonArray = jsonData.getJSONArray("result_message");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                int rangeStart = jsonObject.getInt("range_start");
                int rangeEnd   = jsonObject.getInt("range_end");

                if (rangeStart <= correctAnswersCount && correctAnswersCount <= rangeEnd) {
                    itemResultDialog.setMessage(jsonObject.getString("message"));
                    itemResultDialog.setImage(jsonObject.getString("image"));

                    break;
                }
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return itemResultDialog;
    }
}