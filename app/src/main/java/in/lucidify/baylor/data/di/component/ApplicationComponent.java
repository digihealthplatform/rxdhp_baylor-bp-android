package in.lucidify.baylor.data.di.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import in.lucidify.baylor.BaylorApplication;
import in.lucidify.baylor.data.DataManager;
import in.lucidify.baylor.data.di.ApplicationContext;
import in.lucidify.baylor.data.di.module.ApplicationModule;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BaylorApplication lApplication);

    @ApplicationContext
    Context getContext();

    DataManager getDataManager();
}