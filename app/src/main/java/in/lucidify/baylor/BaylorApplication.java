package in.lucidify.baylor;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;

import com.facebook.stetho.Stetho;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import javax.inject.Inject;

import in.lucidify.baylor.data.DataManager;
import in.lucidify.baylor.data.di.component.ApplicationComponent;
import in.lucidify.baylor.data.di.component.DaggerApplicationComponent;
import in.lucidify.baylor.data.di.module.ApplicationModule;

public class BaylorApplication extends Application {

    private static BaylorApplication instance;

    private static boolean autoPlayAudio;

    protected ApplicationComponent applicationComponent;

    @Inject
    DataManager dataManager;

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(this);

        instance = this;

        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        FlowManager.init(new FlowConfig.Builder(this).build());
    }

    public static BaylorApplication get(Context context) {
        return (BaylorApplication) context.getApplicationContext();
    }

    public static Context getContext() {
        return instance;
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }

    public static void setAutoPlayAudio(boolean autoPlayAudio) {
        BaylorApplication.autoPlayAudio = autoPlayAudio;
    }

    public static boolean getAutoPlayAudio() {
        return autoPlayAudio;
    }
}