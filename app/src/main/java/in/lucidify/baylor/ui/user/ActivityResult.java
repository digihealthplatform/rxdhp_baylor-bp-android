package in.lucidify.baylor.ui.user;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.lucidify.baylor.R;
import in.lucidify.baylor.data.DataManager;
import in.lucidify.baylor.data.adapter.AdapterResult;
import in.lucidify.baylor.data.asset.AssetReader;
import in.lucidify.baylor.data.item.ItemQuestion;
import in.lucidify.baylor.data.item.ItemResultDialog;
import in.lucidify.baylor.ui.BaseActivity;
import in.lucidify.baylor.ui.login.ActivityLogin;
import in.lucidify.baylor.util.LView;

import static in.lucidify.baylor.data.asset.AssetReader.KEY_DONE;
import static in.lucidify.baylor.data.asset.AssetReader.KEY_TITLE_RESULTS;
import static in.lucidify.baylor.data.asset.AssetReader.KEY_YOUR_SCORE;
import static in.lucidify.baylor.util.LConstants.ICONS_PATH;

public class ActivityResult extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.rv_result)
    RecyclerView rvResult;

    @BindView(R.id.iv_menu)
    ImageView ivMenu;

    @BindView(R.id.btn_done)
    Button btnDone;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        tvTitle.setText(AssetReader.getAppText(KEY_TITLE_RESULTS));

        AdapterResult adapter = new AdapterResult(this, AssetReader.getPostQuestions(AssetReader.getJsonData()), AssetReader.getPostQuestionUserAnswers());
        rvResult.setLayoutManager(new LinearLayoutManager(this));
        rvResult.setAdapter(adapter);
        rvResult.setNestedScrollingEnabled(false);

        btnDone.setText(AssetReader.getAppText(KEY_DONE));
        showResultDialog();
    }

    private void showResultDialog() {
        int correctAnswersCount = getTotalCorrectAnswers();
        int totalQuestions      = AssetReader.getPostQuestions(AssetReader.getJsonData()).length;

        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_result, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setView(dialogView);
        final Dialog dialog = builder.create();

        TextView tvScore   = (TextView) dialogView.findViewById(R.id.tv_score);
        TextView tvMessage = (TextView) dialogView.findViewById(R.id.tv_message);

        ImageView ivResult = (ImageView) dialogView.findViewById(R.id.iv_result);

        String result = AssetReader.getAppText(KEY_YOUR_SCORE) + " " + correctAnswersCount + "/" + totalQuestions;
        tvScore.setText(result);

        ItemResultDialog itemResultDialog = AssetReader.getResultDialog(AssetReader.getJsonData(), correctAnswersCount, totalQuestions);

        if (!itemResultDialog.getMessage().equals("")) {
            tvMessage.setText(itemResultDialog.getMessage());
        }

        File userAnsFile = new File(Environment.getExternalStorageDirectory(),
                ICONS_PATH + itemResultDialog.getImage());
        Uri imageUri = Uri.fromFile(userAnsFile);
        LView.loadImage(this, imageUri, ivResult);

        dialogView.findViewById(R.id.iv_close_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private int getTotalCorrectAnswers() {
        int count = 0;

        ItemQuestion[] questions = AssetReader.getPostQuestions(AssetReader.getJsonData());

        int[] userAnswers = AssetReader.getPostQuestionUserAnswers();

        for (int i = 0; i < questions.length; i++) {
            if (questions[i].getCorrectAnswer() == userAnswers[i]) {
                ++count;
            }
        }

        return count;
    }

    @OnClick(R.id.btn_done)
    void startNew() {
        Intent intent = new Intent(ActivityResult.this, ActivityThankYou.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
    }

    @OnClick(R.id.iv_menu)
    void onMenuClick() {
        PopupMenu popup = new PopupMenu(this, ivMenu);

        popup.getMenuInflater()
                .inflate(R.menu.menu_default, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityResult.this, R.style.AlertDialogLight);

                builder.setMessage(getString(R.string.confirm_logout));
                builder.setPositiveButton(getString(R.string.btn_logout), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dataManager.logout();
                        startActivity(new Intent(ActivityResult.this, ActivityLogin.class));
                        finish();
                    }
                });
                builder.setNegativeButton(getString(R.string.btn_cancel), null);
                builder.show();

                return true;
            }
        });

        popup.show();
    }
}