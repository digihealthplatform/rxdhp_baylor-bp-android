package in.lucidify.baylor.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import java.io.File;

import es.dmoral.toasty.Toasty;
import in.lucidify.baylor.BaylorApplication;
import in.lucidify.baylor.R;
import in.lucidify.baylor.data.asset.AssetReader;
import in.lucidify.baylor.data.di.component.ActivityComponent;
import in.lucidify.baylor.data.di.component.DaggerActivityComponent;
import in.lucidify.baylor.data.di.module.ActivityModule;
import in.lucidify.baylor.ui.user.ActivityLanguageSelection;
import in.lucidify.baylor.util.LLog;

import static in.lucidify.baylor.util.LConstants.AUDIO_PATH;

public class BaseActivity extends AppCompatActivity {

    int currentAudioFile = 0;

    private boolean isAudioPlaying;

    private String[] audioFiles;

    protected ActivityComponent activityComponent;
    private   MediaPlayer       audioPlayer;
    private   ImageView         ivPlayAudio;

    protected ActivityComponent getActivityComponent(Activity activity) {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(activity))
                    .applicationComponent(BaylorApplication.get(activity).getComponent())
                    .build();
        }
        return activityComponent;
    }

    @Override
    public void onBackPressed() {
        confirmBackPressed();
    }

    protected void confirmBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getAppText(AssetReader.KEY_CONFIRM_ON_BACK));
        builder.setPositiveButton(AssetReader.getAppText(AssetReader.KEY_YES), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                BaseActivity.super.onBackPressed();
                Intent intent = new Intent(BaseActivity.this, ActivityLanguageSelection.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });
        builder.setNegativeButton(AssetReader.getAppText(AssetReader.KEY_NO), null);
        builder.show();
    }

    protected void initAudio(ImageView ivPlayAudio, String audioFileName) {
        stopAudio();

        ivPlayAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAudioPlaying) {
                    BaylorApplication.setAutoPlayAudio(false);
                    stopAudio();
                } else {
                    BaylorApplication.setAutoPlayAudio(true);
                    playAudio();
                }
            }
        });

        if (null == audioFileName || audioFileName.isEmpty()) {
            audioFiles = null;
            return;
        }

        this.ivPlayAudio = ivPlayAudio;

        this.audioFiles = audioFileName.split(",");
        currentAudioFile = 0;

        if (BaylorApplication.getAutoPlayAudio()) {
            playAudio();
        }
    }

    protected void playAudio() {
        if (null == audioFiles) {
            return;
        }

        final File audioFile = new File(Environment.getExternalStorageDirectory(), AUDIO_PATH + audioFiles[currentAudioFile]);

        if (!audioFile.exists()) {
            Toasty.warning(this, "Audio file doesn't exit").show();
            BaylorApplication.setAutoPlayAudio(false);
            return;
        }
        ivPlayAudio.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.playing_audio));
        audioPlayer = MediaPlayer.create(this, Uri.fromFile(audioFile));

        try {
            audioPlayer.start();
            audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    ++currentAudioFile;

                    if (null == audioFiles) {
                        return;
                    }

                    if (currentAudioFile >= audioFiles.length) {
                        ivPlayAudio.setImageDrawable(ContextCompat.getDrawable(BaseActivity.this, R.drawable.play_audio));
                        currentAudioFile = 0;
                        isAudioPlaying = false;
                    } else {
                        playAudio();
                    }
                }
            });
        } catch (Exception e) {
        }

        isAudioPlaying = true;
    }

    protected void stopAudio() {
        try {
            audioPlayer.stop();
            ivPlayAudio.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.play_audio));
            isAudioPlaying = false;
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }
}