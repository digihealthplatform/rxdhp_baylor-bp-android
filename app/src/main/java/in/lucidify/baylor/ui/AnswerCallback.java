package in.lucidify.baylor.ui;

public interface AnswerCallback {
    void onAnswerSelected(int position);
}