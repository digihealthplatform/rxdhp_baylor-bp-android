package in.lucidify.baylor.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.lucidify.baylor.BaylorApplication;
import in.lucidify.baylor.R;
import in.lucidify.baylor.data.DataManager;
import in.lucidify.baylor.data.asset.AssetReader;
import in.lucidify.baylor.ui.BaseActivity;
import in.lucidify.baylor.ui.admin.ActivityAdminHome;
import in.lucidify.baylor.ui.user.ActivityLanguageSelection;

public class ActivityLogin extends BaseActivity {

    private boolean userLogin = true;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.et_username)
    EditText etUsername;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.tv_login_user)
    TextView tvLoginUser;

    @BindView(R.id.tv_login_admin)
    TextView tvLoginAdmin;

    @BindView(R.id.ll_user)
    LinearLayout llUser;

    @BindView(R.id.ll_admin)
    LinearLayout llAdmin;

    @BindView(R.id.cb_remember_me)
    CheckBox cbRememberMe;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        tvTitle.setText(getString(R.string.title_login));

        boolean initSuccess = AssetReader.init();
        if (!initSuccess) {
            finish();
        }
    }

    @OnClick(R.id.ll_admin)
    void adminLogin() {
        if (!userLogin) {
            return;
        }

        userLogin = false;

        tvLoginUser.setTextColor(ContextCompat.getColor(this, R.color.color_grey_dark));
        tvLoginAdmin.setTextColor(ContextCompat.getColor(this, R.color.white));

        llUser.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        llAdmin.setBackgroundColor(ContextCompat.getColor(this, R.color.green));

        btnLogin.setText(getString(R.string.login_as_admin));

        etUsername.setVisibility(View.GONE);

        clearFields();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @OnClick(R.id.ll_user)
    void userLogin() {
        if (userLogin) {
            return;
        }
        userLogin = true;

        tvLoginUser.setTextColor(ContextCompat.getColor(this, R.color.white));
        tvLoginAdmin.setTextColor(ContextCompat.getColor(this, R.color.color_grey_dark));

        llUser.setBackgroundColor(ContextCompat.getColor(this, R.color.green));
        llAdmin.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        btnLogin.setText(getString(R.string.login_as_user));

        etUsername.setVisibility(View.VISIBLE);

        clearFields();

        cbRememberMe.setChecked(true);
    }

    private void clearFields() {
        etUsername.setText("");
        etPassword.setText("");
        cbRememberMe.setChecked(false);
        etUsername.requestFocus();
    }

    @OnClick(R.id.btn_login)
    void login() {

        if (!userLogin) {
            if (etPassword.getText().toString().equals("baylor123")) {
                dataManager.adminLoggedIn(cbRememberMe.isChecked());
                startActivity(new Intent(this, ActivityAdminHome.class));
                finish();
            } else {
                Toasty.warning(BaylorApplication.getContext(), BaylorApplication.getContext().getString(R.string.invalid_credentials),
                        Toast.LENGTH_SHORT).show();
            }
            return;
        }

        if (dataManager.validateUserLogin(etUsername.getText().toString(), etPassword.getText().toString())) {
            dataManager.userLoggedIn(cbRememberMe.isChecked(), etUsername.getText().toString());
            startActivity(new Intent(this, ActivityLanguageSelection.class));
            finish();

        } else {
            Toasty.warning(BaylorApplication.getContext(), BaylorApplication.getContext().getString(R.string.invalid_credentials),
                    Toast.LENGTH_SHORT).show();
        }
    }
}