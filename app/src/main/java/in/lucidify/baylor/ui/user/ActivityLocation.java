package in.lucidify.baylor.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.lucidify.baylor.R;
import in.lucidify.baylor.data.asset.AssetReader;
import in.lucidify.baylor.ui.BaseActivity;
import in.lucidify.baylor.util.LToast;

import static in.lucidify.baylor.data.asset.AssetReader.KEY_CONTINUE;
import static in.lucidify.baylor.data.asset.AssetReader.KEY_ENTER_LOCATION_NAME;
import static in.lucidify.baylor.data.asset.AssetReader.KEY_LOCATION_NAME;
import static in.lucidify.baylor.data.asset.AssetReader.KEY_LOCATION_TYPE;
import static in.lucidify.baylor.data.asset.AssetReader.KEY_SELECT_LOCATION_TYPE;
import static in.lucidify.baylor.data.asset.AssetReader.KEY_TITLE_LOCATION;
import static in.lucidify.baylor.util.LView.setSpinnerAdapter;

public class ActivityLocation extends BaseActivity {

    private String locationType;

    private String[] locationTypes;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_location_type)
    TextView tvLocationType;

    @BindView(R.id.sp_location)
    Spinner spLocation;

    @BindView(R.id.tv_location_name)
    TextView tvLocationName;

    @BindView(R.id.et_location_name)
    EditText etLocationName;

    @BindView(R.id.btn_continue)
    Button btnContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        ButterKnife.bind(this);
        tvTitle.setText(AssetReader.getAppText(KEY_TITLE_LOCATION));

        tvLocationName.setText(AssetReader.getAppText(KEY_LOCATION_NAME) + ": ");
        tvLocationType.setText(AssetReader.getAppText(KEY_LOCATION_TYPE) + ": ");
        btnContinue.setText(AssetReader.getAppText(KEY_CONTINUE));

        locationTypes = AssetReader.getLocationTypes(AssetReader.getJsonData());

        setSpinnerAdapter(this, spLocation, locationTypes);

        spLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position > 0) {
                    locationType = locationTypes[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @OnClick(R.id.btn_continue)
    void onContinue() {

        if (null == locationType) {
            LToast.warning(AssetReader.getAppText(KEY_SELECT_LOCATION_TYPE));

        } else if (etLocationName.getText().toString().equals("")) {
            LToast.warning(AssetReader.getAppText(KEY_ENTER_LOCATION_NAME));

        } else {
            AssetReader.saveLocationDetails(locationType, etLocationName.getText().toString());
            startActivity(new Intent(this, ActivitySociodemographicData.class));
        }
    }
}