package in.lucidify.baylor.ui.user;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.lucidify.baylor.R;
import in.lucidify.baylor.data.asset.AssetReader;
import in.lucidify.baylor.ui.BaseActivity;

import static in.lucidify.baylor.data.asset.AssetReader.KEY_START_NEW;
import static in.lucidify.baylor.data.asset.AssetReader.KEY_TITLE_THANK_YOU;
import static in.lucidify.baylor.util.LConstants.ICONS_PATH;

public class ActivityThankYou extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.iv_thank_you)
    ImageView ivThankYou;

    @BindView(R.id.btn_start_new)
    Button btnStartNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        ButterKnife.bind(this);
        tvTitle.setText(AssetReader.getAppText(KEY_TITLE_THANK_YOU));

        File imageFile = new File(Environment.getExternalStorageDirectory(), ICONS_PATH + "monkey.gif");

        if (imageFile.exists()) {
            Uri imageUri = Uri.fromFile(imageFile);
            Glide.with(this)
                    .load(imageUri)
                    .asGif()
                    .into(ivThankYou);
        }

        btnStartNew.setText(AssetReader.getAppText(KEY_START_NEW));
    }

    @OnClick(R.id.btn_start_new)
    void onStartNew() {
        Intent intent = new Intent(ActivityThankYou.this, ActivityLanguageSelection.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    @Override
    public void onBackPressed() {
//        finish();
    }
}