package in.lucidify.baylor.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import in.lucidify.baylor.data.DataManager;
import in.lucidify.baylor.ui.BaseActivity;
import in.lucidify.baylor.ui.admin.ActivityAdminHome;
import in.lucidify.baylor.ui.user.ActivityLanguageSelection;

import static in.lucidify.baylor.data.database.SharedPrefsHelper.ADMIN_LOGGED_IN;
import static in.lucidify.baylor.data.database.SharedPrefsHelper.KEY_LOGGED_IN;
import static in.lucidify.baylor.data.database.SharedPrefsHelper.KEY_REMEMBER_LOG_IN;
import static in.lucidify.baylor.data.database.SharedPrefsHelper.NOT_LOGGED_IN;
import static in.lucidify.baylor.data.database.SharedPrefsHelper.USER_LOGGED_IN;

public class ActivityLoginCheck extends BaseActivity {

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent(this).inject(this);

        if (dataManager.isCopyAssetsNeeded()) {
            startActivity(new Intent(this, ActivityCopyAssets.class));
        } else {
            decideLogin();
        }

        finish();
    }

    private void decideLogin() {

        Intent intent = new Intent(this, ActivityLogin.class);

        int loginState = dataManager.getIntPref(KEY_LOGGED_IN, NOT_LOGGED_IN);

        switch (loginState) {
            case NOT_LOGGED_IN: {
                intent = new Intent(this, ActivityLogin.class);
            }
            break;

            case USER_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityLanguageSelection.class);
                }
            }
            break;

            case ADMIN_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityAdminHome.class);
                }
            }
            break;
        }

        startActivity(intent);
    }
}