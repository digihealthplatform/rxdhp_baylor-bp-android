package in.lucidify.baylor.ui.login;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import in.lucidify.baylor.R;
import in.lucidify.baylor.data.DataManager;
import in.lucidify.baylor.data.asset.CopyAssetsTask;
import in.lucidify.baylor.ui.BaseActivity;
import in.lucidify.baylor.util.LToast;

import static in.lucidify.baylor.util.LConstants.AUDIO_PATH;
import static in.lucidify.baylor.util.LConstants.CODE_WRITE_PERMISSION;
import static in.lucidify.baylor.util.LConstants.FILES_PATH_HIDDEN;
import static in.lucidify.baylor.util.LConstants.ICONS_PATH;
import static in.lucidify.baylor.util.LConstants.INPUT_JSON_PATH;
import static in.lucidify.baylor.util.LConstants.OUTPUT_JSON_PATH;
import static in.lucidify.baylor.util.LConstants.VIDEO_PATH;

public class ActivityCopyAssets extends BaseActivity implements CopyAssetsTask.CallBack {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_copy_assets);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        tvTitle.setText(getString(R.string.app_name));

        createFolderWithPermissionCheck();
    }

    @Override
    public void onBackPressed() {
    }

    private void createFolderWithPermissionCheck() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (PackageManager.PERMISSION_GRANTED != permissionCheck) {

            ActivityCompat.requestPermissions((Activity) this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    CODE_WRITE_PERMISSION);
        } else {
            createFolders();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case CODE_WRITE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createFolders();
                } else {
                    Toasty.warning(this, getString(R.string.message_permission_needed), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
            break;
        }
    }

    private void createFolders() {

        createFolder(VIDEO_PATH);
        createFolder(AUDIO_PATH);
        createFolder(OUTPUT_JSON_PATH);
        createFolder(FILES_PATH_HIDDEN);
        createFolder(ICONS_PATH);
        createFolder(INPUT_JSON_PATH);

        new CopyAssetsTask(this, this).execute();
    }

    private void createFolder(String folderPath) {

        File filePath = new File(Environment.getExternalStorageDirectory(), folderPath);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }
    }

    @Override
    public void onAssetCopied(boolean copySuccess) {

        if (copySuccess) {
            dataManager.onAssetsCopied();
            startActivity(new Intent(this, ActivityLoginCheck.class));

        } else {
            LToast.warning(getString(R.string.invalid_apk));
        }

        finish();
    }
}