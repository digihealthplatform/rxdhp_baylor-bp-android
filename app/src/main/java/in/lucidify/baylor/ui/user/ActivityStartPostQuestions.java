package in.lucidify.baylor.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.lucidify.baylor.R;
import in.lucidify.baylor.data.asset.AssetReader;
import in.lucidify.baylor.ui.BaseActivity;

import static in.lucidify.baylor.data.asset.AssetReader.KEY_START_POST_QUESTIONS;
import static in.lucidify.baylor.data.asset.AssetReader.KEY_START_POST_QUESTIONS_AUDIO_FILE;
import static in.lucidify.baylor.data.asset.AssetReader.KEY_TITLE_POST_QUESTIONS;

public class ActivityStartPostQuestions extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_message)
    TextView tvMessage;

    @BindView(R.id.iv_next)
    ImageView ivNext;

    @BindView(R.id.iv_play_audio)
    ImageView ivPlayAudio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_post_questions);
        ButterKnife.bind(this);

        tvTitle.setText(AssetReader.getAppText(KEY_TITLE_POST_QUESTIONS));
        tvMessage.setText(AssetReader.getAppText(KEY_START_POST_QUESTIONS));
        ivNext.setVisibility(View.VISIBLE);

        initAudio(ivPlayAudio, AssetReader.getAppText(KEY_START_POST_QUESTIONS_AUDIO_FILE));
    }

    @Override
    protected void onStop() {
        stopAudio();
        super.onStop();
    }

    @OnClick(R.id.iv_next)
    void onNext() {
        startActivity(new Intent(this, ActivityPostQuestions.class));
    }
}