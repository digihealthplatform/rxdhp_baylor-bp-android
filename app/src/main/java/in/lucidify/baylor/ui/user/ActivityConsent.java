package in.lucidify.baylor.ui.user;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.lucidify.baylor.R;
import in.lucidify.baylor.data.adapter.AdapterConsent;
import in.lucidify.baylor.data.asset.AssetReader;
import in.lucidify.baylor.data.item.ConsentData;
import in.lucidify.baylor.ui.BaseActivity;
import in.lucidify.baylor.util.LView;

import static in.lucidify.baylor.util.LConstants.ICONS_PATH;

public class ActivityConsent extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.rv_consent)
    RecyclerView rvConsent;

    @BindView(R.id.iv_play_audio)
    ImageView ivPlayAudio;

    @BindView(R.id.iv_yes)
    ImageView ivYes;

    @BindView(R.id.iv_no)
    ImageView ivNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consent);
        ButterKnife.bind(this);
        ConsentData consentData = AssetReader.getConsent();
        tvTitle.setText(consentData.getTitle());
        AdapterConsent adapterConsent = new AdapterConsent(this, consentData.getPoints());
        rvConsent.setLayoutManager(new LinearLayoutManager(this));
        rvConsent.setAdapter(adapterConsent);

        File imageFile = new File(Environment.getExternalStorageDirectory(), ICONS_PATH + consentData.getAgreeIcon());

        if (imageFile.exists()) {
            Uri imageUri = Uri.fromFile(imageFile);
            LView.loadImage(this, imageUri, ivYes);
        }

        imageFile = new File(Environment.getExternalStorageDirectory(), ICONS_PATH + consentData.getCancelIcon());

        if (imageFile.exists()) {
            Uri imageUri = Uri.fromFile(imageFile);
            LView.loadImage(this, imageUri, ivNo);
        }

        AssetReader.surveyStarted();

        initAudio(ivPlayAudio, consentData.getAudioFile());
    }

    @OnClick(R.id.iv_yes)
    void onYes() {
        startActivity(new Intent(this, ActivityLocation.class));
        finish();
    }

    @Override
    protected void onStop() {
        stopAudio();
        super.onStop();
    }

    @OnClick(R.id.iv_no)
    void onNo() {
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}